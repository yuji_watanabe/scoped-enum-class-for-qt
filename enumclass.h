﻿#ifndef ENUMCLASS_H
#define ENUMCLASS_H

#include <QMap>
#include <QVector>
#include <QString>
#include <QStringList>

#define EnumClass(classname, ...) \
template <typename T = int> \
class classname { \
public: \
    enum type {__VA_ARGS__}; \
    static const QString &metaData() { \
        static const QString metaData = #__VA_ARGS__; \
        return metaData; \
    } \
    static int size() { \
        return createEnumMap().size(); \
    } \
    static int filteredSize() { \
        int size = 0; \
        createEnumMap(&size); \
        return size; \
    } \
    static QMap<T, QString> map() { \
        return createEnumMap(); \
    } \
    QString toString() { \
        return createEnumMap()[static_cast<T>(m_type)]; \
    } \
    static QString toString(const type &t) { \
        QMap<T, QString> map = createEnumMap(); \
        if (!map.contains(static_cast<T>(t))) { \
            return ""; \
        } \
        return map[static_cast<T>(t)]; \
    } \
    static QString toString(const classname &t) { \
        QMap<T, QString> map = createEnumMap(); \
        if (!map.contains(static_cast<T>(t.m_type))) { \
            return ""; \
        } \
        return map[static_cast<T>(t.m_type)]; \
    } \
    static T fromString(const QString &value) { \
        int filteredSize = 0; \
        int unknownKey = 0; \
        QMap<T, QString> map = createEnumMap(&filteredSize, &unknownKey); \
        foreach (QString s, map.values()) { \
            if (s == value) { \
                return map.key(value); \
            } \
        } \
        return unknownKey; \
    } \
    classname() : m_type(), m_value() {} \
    classname(type value) : m_type(value), m_value(static_cast<T>(value)) {} \
    classname(T value) : m_type(static_cast<type>(value)), m_value(value) {} \
    bool operator==(const type &other) const { return m_type == other; } \
    bool operator!=(const type &other) const { return m_type != other; } \
    bool operator==(const classname &other) const { return m_type == other.m_type; } \
    bool operator!=(const classname &other) const { return m_type != other.m_type; } \
    bool operator<(const type &other) const { return static_cast<T>(m_type) < static_cast<T>(other); } \
    bool operator<=(const type &other) const { return static_cast<T>(m_type) <= static_cast<T>(other); } \
    bool operator>(const type &other) const { return static_cast<T>(m_type) > static_cast<T>(other); } \
    bool operator>=(const type &other) const { return static_cast<T>(m_type) >= static_cast<T>(other); } \
    bool operator<(const classname &other) const { return m_value < other.m_value; } \
    bool operator<=(const classname &other) const { return m_value <= other.m_value; } \
    bool operator>(const classname &other) const { return m_value > other.m_value: } \
    bool operator>=(const classname &other) const { return m_value >= other.m_value; } \
    operator type() const { return m_type; } \
private: \
    type m_type; \
    T m_value; \
    /*int m_filteredSize; \
    T m_unknownKey; \
    QMap<T, QString> m_map;*/ \
private: \
    static QMap<T, QString> createEnumMap(int *filteredSize = 0, int *unknownKey = 0) { \
        static int size = 0; \
        static T key = 0; \
        static QMap<T, QString> map; \
        if (map.isEmpty()) { \
            QStringList list = metaData().split(","); \
            size = list.size(); \
            foreach (QString str, list) { \
                QStringList tmp = str.split("="); \
                if (tmp.size() > 1) { \
                    QString num(tmp.at(1).trimmed()); \
                    bool ok; \
                    if (num.indexOf("0x") >= 0) { \
                        key = static_cast<T>(num.toULong(&ok, 16)); \
                    } else { \
                        num.toInt(&ok); \
                        if (ok) { \
                            key = static_cast<T>(num.toInt()); \
                        } else { \
                            --size; \
                        } \
                    } \
                } \
                map[key++] = tmp.at(0).trimmed(); \
            } \
        } \
        if (unknownKey) { \
            *unknownKey = key; \
        } \
        if (filteredSize) { \
            *filteredSize = size; \
        } \
        return map; \
    } \
    /*QMap<T, QString> enumMap() const { \
        m_map = createEnumMap(m_filteredSize, m_unknownKey); \
        return m_map; \
    }*/ \
}; \

#endif // ENUMCLASS_H
